# webview

Python extension that provides API for the [webview] library.

## Getting started

Install the bindings:

```bash
pip install webview
```

Try the following example:

```python
import webview

w = webview.Webview(debug=False)
w.set_size(width=320, height=240, hints=webview.Hint.NONE)
w.set_title(title="Hello")
w.navigate(url="https://google.com")
w.run()
```

## Local development

You need to pull in the main webview library as a submodule:

```shell
$ git submodule update --init
```

Then, you can run:

```shell
$ pip install -e .
```

If you are on linux, you will need development headers for `gtk+-3.0`
and `webkit2gtk-4.0`.

For debian based distributions they can be installed with:

```shell
$ sudo apt install libgtk-3-dev libwebkit2gtk-4.0-dev
```

Tests can be run with:

```shell
$ make test
```

You can lint with:

```shell
$ make lint
```

Linting the python requires you to install the dependencies listed in
requirements_dev.txt.

```shell
$ pip install -r requirements_dev.txt
```

Linting the c++ code requires `clang-format` to be installed, which
usually comes installed with `clang`. On macos, though, it does not.
It can be installed through `brew` via `brew install llvm`.

Tests and linting can be run in docker as well.

```shell
$ make docker_lint docker_test
```

[webview]: https://github.com/webview/webview
