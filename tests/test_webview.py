from unittest import TestCase

import webview


class TestWebview(TestCase):
    def test_bindings(self):
        value = 0

        wv = webview.Webview()

        def increment_value():
            nonlocal value
            value += 1

        wv.set_bindings(increment_value=increment_value, terminate=wv.terminate)
        wv.set_html(
            """<script>
    increment_value().then(() => terminate());
    </script>
    """
        )

        wv.run()

        self.assertEqual(value, 1)

    def test_init(self):
        value = 100

        wv = webview.Webview()

        def decrement_value():
            nonlocal value
            value -= 1

        wv.bind("decrement_value", decrement_value)
        wv.bind("terminate", wv.terminate)
        wv.init("decrement_value().then(() => terminate())")
        wv.navigate("data:text/html,<html></html>")

        wv.run()

        self.assertEqual(value, 99)

    def test_eval(self):
        value = -1

        wv = webview.Webview()

        def javascript_terminate():
            nonlocal value
            value += 1
            wv.eval("terminate()")

        wv.set_bindings(
            javascript_terminate=javascript_terminate, terminate=wv.terminate
        )
        wv.navigate("data:text/html,<script>javascript_terminate();</script>")

        wv.run()

        self.assertEqual(value, 0)
