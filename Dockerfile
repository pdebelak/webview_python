FROM quay.io/pypa/manylinux2014_x86_64
RUN yum update && \
    yum install -y \
    clang \
    gtk3-devel \
    webkitgtk4-devel \
    xorg-x11-server-Xvfb
ARG python_version=38
ENV PATH="/opt/python/cp${python_version}-cp${python_version}/bin:${PATH}"

WORKDIR /webview/build

COPY requirements_dev.txt .
RUN pip install -r requirements_dev.txt

COPY submodules submodules
COPY scripts scripts
COPY examples examples
COPY setup.cfg .
COPY setup.py .
COPY webview webview
COPY tests tests

RUN pip install .

WORKDIR /webview
