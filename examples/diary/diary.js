class DiaryApp extends HTMLElement {
  constructor() {
    super();
    window.addEventListener('hashchange', () => this.route(location.hash.slice(1)));
    this.route('');
  }

  route(location) {
    if (location === 'new') {
      this.newEntry();
    } else if (location === 'quit') {
      quit();
    } else if (location.match(/^entry-\d+$/)) {
      const id = parseInt(location.split('-')[1], 10);
      this.showEntry(id);
    } else if (location.match(/^entry-\d+-edit$/)) {
      const id = parseInt(location.split('-')[1], 10);
      this.updateEntry(id);
    } else if (location.match(/^entry-\d+-delete$/)) {
      const id = parseInt(location.split('-')[1], 10);
      delete_entry(id).then(() => { window.location.hash = '#'; });
    } else {
      this.listEntries();
    }
  }

  async listEntries() {
    this.innerHTML = `
<h1>Entries</h1>
<p><a href="#new">New Entry</a></p>
<ul></ul>
`;
    const entries = await list();
    const ul = this.querySelector('ul');
    entries.forEach(entry => {
      const li = document.createElement('li');
      const a = document.createElement('a');
      a.href = `#entry-${entry.id}`;
      a.textContent = entry.title;
      li.appendChild(a);
      ul.appendChild(li);
    });
  }

  newEntry() {
    this.innerHTML = `
<h1>New Entry</h1>
<form id="new-entry-form">
  <p><label for="title">Title</label></p>
  <p><input name="title" value=""></p>
  <p><label for="body">Body</label></p>
  <p><div class="editor" contenteditable></div></p>
  <button>Save</button>
</form>
`;
    document.getElementById('new-entry-form').addEventListener('submit', (e) => {
      e.preventDefault();
      const form = new FormData(e.target);
      const title = form.get('title');
      const body = e.target.querySelector('.editor').innerHTML;
      if (!(title && body)) {
        return;
      }
      save(title, body).then(id => {
        this.showEntry(id);
      });
    });
  }

  async showEntry(id) {
    const entry = await show(id);
    this.innerHTML = `
<h1>${entry.title}</h1>
<p>Written ${entry.created_at} and updated ${entry.updated_at}</p>
<p><a href="#entry-${entry.id}-edit">Edit entry</a></p>
<div class="entry-body">
  ${entry.body}
</div>
`;
  }

  async updateEntry(id) {
    const entry = await show(id);
    this.innerHTML = `
<h1>Update Entry</h1>
<p><a href="#entry-${entry.id}">Show entry</a></p>
<form id="update-entry-form">
  <p><label for="title">Title</label></p>
  <p><input name="title" value="${entry.title}"></p>
  <p><label for="body">Body</label></p>
  <p><div class="editor" contenteditable>${entry.body}</div></p>
  <button>Save</button>
  <a href="#entry-${entry.id}-delete">Delete</a>
</form>
`;
    document.getElementById('update-entry-form').addEventListener('submit', (e) => {
      e.preventDefault();
      const form = new FormData(e.target);
      const title = form.get('title');
      const body = e.target.querySelector('.editor').innerHTML;
      if (!(title && body)) {
        return;
      }
      update(entry.id, title, body).then(() => {
        this.showEntry(entry.id);
      });
    });
  }
}
customElements.define('diary-app', DiaryApp);
