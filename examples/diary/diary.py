"""
A complicated example showing usage of Webview.
"""
import sqlite3
from pathlib import Path

from webview import Hint, Webview

directory = Path(__file__).parent


class App:
    def __init__(self):
        self.conn = sqlite3.connect(directory.joinpath("diary.db"))
        self.conn.execute(
            """
CREATE TABLE IF NOT EXISTS entries (
  id INTEGER NOT NULL PRIMARY KEY,
  title STRING NOT NULL,
  body TEXT NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
)"""
        )
        self.wv = Webview(debug=True)

    def start(self):
        self.wv.set_title("Your Diary")
        self.wv.set_size(800, 600, Hint.NONE)

        with directory.joinpath("diary.css").open() as f:
            css = f.read()
        with directory.joinpath("diary.js").open() as f:
            js = f.read()
        with directory.joinpath("diary.html").open() as f:
            html = f.read()
        self.wv.set_html(html.format(css=css, js=js))
        self.wv.set_bindings(
            list=self.list_entries,
            save=self.save_entry,
            show=self.show_entry,
            update=self.update_entry,
            delete_entry=self.delete_entry,
            quit=self.quit_app,
        )
        self.wv.run()

    def list_entries(self):
        results = self.conn.execute(
            "SELECT id, title FROM entries ORDER BY created_at DESC"
        ).fetchall()
        output = [{"id": r[0], "title": r[1]} for r in results]
        return output

    def save_entry(self, title, body):
        with self.conn:
            cursor = self.conn.execute(
                """
INSERT INTO entries (title, body)
VALUES (?, ?)""",
                (title, body),
            )
            return cursor.lastrowid

    def show_entry(self, id):
        result = self.conn.execute(
            "SELECT id, title, body, created_at, updated_at FROM entries WHERE id = ?",
            (id,),
        ).fetchone()
        return {
            "id": result[0],
            "title": result[1],
            "body": result[2],
            "created_at": result[3],
            "updated_at": result[4],
        }

    def update_entry(self, id, title, body):
        with self.conn:
            self.conn.execute(
                """
UPDATE entries
SET title = ?, body = ?, updated_at = CURRENT_TIMESTAMP
WHERE id = ?""",
                (title, body, id),
            )

    def delete_entry(self, id):
        with self.conn:
            self.conn.execute("DELETE FROM entries WHERE id = ?", (id,))

    def quit_app(self):
        self.wv.terminate()


if __name__ == "__main__":
    app = App()
    app.start()
