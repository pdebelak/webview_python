# Diary

A simple diary used as an example slightly more complicated
webview_python application.

It features a sqlite3 database to store entries and allows you to
create and edit diary entries on your computer.
