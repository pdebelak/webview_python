"""
A basic example showing usage of Webview.
"""
from webview import Hint, Webview

w = Webview(debug=True)
w.set_title("Minimal webview example")
w.set_size(800, 600, Hint.NONE)


def test_func1(*args):
    print(*args)
    return "Hello from python!"


def test_func2(*args):
    print(*args)
    return {"test": "object"}


def do_quit():
    w.terminate()


w.set_bindings(test_func1=test_func1, test_func2=test_func2, quit=do_quit)
w.navigate(
    """data:text/html,<html>
<head>
  <title>My Title</title>
</head>
<body>
  <h1 id="doc-title">Test</h1>
  <button id="quit-button">Quit</button>
  <script>
    document.getElementById('quit-button').addEventListener('click', () => quit());
    test_func1('it out').then(resp => {
      document.getElementById('doc-title').textContent = resp;
      test_func2('another').then(resp => {
        var p = document.createElement('p');
        p.textContent = `Test = ${resp.test}`;
        document.body.appendChild(p);
      });
    });
  </script>
</body>
</html>"""
)
w.run()
