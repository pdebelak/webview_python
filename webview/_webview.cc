/*
 * MIT License
 *
 * Copyright (c) 2021 Peter Debelak
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#define PY_SSIZE_T_CLEAN
#include <Python.h>

#include "../submodules/webview/webview.h"

typedef struct {
  webview_t webview;
  PyObject *func;
} webview_bind_context;

typedef struct webview_bind_contexts {
  webview_bind_context *context;
  struct webview_bind_contexts *next;
} webview_bind_contexts;

typedef struct {
  PyObject_HEAD webview_t webview;
  webview_bind_contexts *contexts;
} WebviewObject;

static PyObject *Webview_new(PyTypeObject *type, PyObject *args,
                             PyObject *kwds) {
  WebviewObject *self;
  self = PyObject_New(WebviewObject, type);
  if (self == NULL)
    return NULL;
  self->webview = NULL;
  self->contexts = NULL;
  return (PyObject *)self;
}

static int Webview_init(WebviewObject *self, PyObject *args, PyObject *kwds) {
  int debug;
  static char *kwlist[] = {(char *)"debug", NULL };
  if (!PyArg_ParseTupleAndKeywords(args, kwds, "|i", kwlist, &debug))
    return -1;

  webview_t webview = webview_create(debug, NULL);
  if (webview == NULL)
    return -1;

  self->webview = webview;
  return 0;
}

static void Webview_dealloc(WebviewObject *self) {
  webview_destroy(self->webview);
  webview_bind_contexts *head = self->contexts;
  webview_bind_contexts *tmp;
  while (head != NULL) {
    Py_DECREF(head->context->func);
    free(head->context);
    tmp = head;
    head = head->next;
    free(tmp);
  }
  PyObject_Free(self);
}

static PyObject *Webview_run(WebviewObject *self) {
  webview_run(self->webview);
  Py_RETURN_NONE;
}

static PyObject *Webview_navigate(WebviewObject *self, PyObject *args,
                                  PyObject *kwds) {
  const char *url;
  static char *kwlist[] = {(char *)"url", NULL };
  if (!PyArg_ParseTupleAndKeywords(args, kwds, "s", kwlist, &url))
    return NULL;
  webview_navigate(self->webview, url);
  Py_RETURN_NONE;
}

static PyObject *Webview_set_title(WebviewObject *self, PyObject *args,
                                   PyObject *kwds) {
  const char *title;
  static char *kwlist[] = {(char *)"title", NULL };
  if (!PyArg_ParseTupleAndKeywords(args, kwds, "s", kwlist, &title))
    return NULL;
  webview_set_title(self->webview, title);
  Py_RETURN_NONE;
}

static PyObject *Webview_set_size(WebviewObject *self, PyObject *args,
                                  PyObject *kwds) {
  int width, height, hints;
  static char *kwlist[] = {(char *)"width", (char *)"height",
                           (char *)"hints", NULL };
  if (!PyArg_ParseTupleAndKeywords(args, kwds, "iii", kwlist, &width, &height,
                                   &hints))
    return NULL;
  webview_set_size(self->webview, width, height, hints);
  Py_RETURN_NONE;
}

static PyObject *Webview_set_html(WebviewObject *self, PyObject *args,
                                  PyObject *kwds) {
  const char *html;
  static char *kwlist[] = {(char *)"html", NULL };
  if (!PyArg_ParseTupleAndKeywords(args, kwds, "s", kwlist, &html))
    return NULL;
  webview_set_html(self->webview, html);
  Py_RETURN_NONE;
}

static PyObject *Webview_terminate(WebviewObject *self) {
  webview_terminate(self->webview);
  Py_RETURN_NONE;
}

static PyObject *Webview_run_init(WebviewObject *self, PyObject *args,
                                  PyObject *kwds) {
  const char *js;
  static char *kwlist[] = {(char *)"js", NULL };
  if (!PyArg_ParseTupleAndKeywords(args, kwds, "s", kwlist, &js))
    return NULL;
  webview_init(self->webview, js);
  Py_RETURN_NONE;
}

static PyObject *Webview_eval(WebviewObject *self, PyObject *args,
                              PyObject *kwds) {
  const char *js;
  static char *kwlist[] = {(char *)"js", NULL };
  if (!PyArg_ParseTupleAndKeywords(args, kwds, "s", kwlist, &js))
    return NULL;
  webview_eval(self->webview, js);
  Py_RETURN_NONE;
}

static void run_dispatch_function(webview_t w, void *arg) {
  PyObject *func = (PyObject *)arg;
  PyObject_CallObject(func, NULL);
  Py_DECREF(func);
}

static PyObject *Webview_dispatch(WebviewObject *self, PyObject *args,
                                  PyObject *kwds) {
  PyObject *func;
  static char *kwlist[] = {(char *)"func", NULL };
  if (!PyArg_ParseTupleAndKeywords(args, kwds, "O", kwlist, &func))
    return NULL;
  if (!PyCallable_Check(func)) {
    PyErr_SetString(PyExc_TypeError, "func parameter must be callable");
    return NULL;
  }
  Py_XINCREF(func);
  webview_dispatch(self->webview, run_dispatch_function, (void *)func);
  Py_RETURN_NONE;
}

static void run_bind_func(const char *seq, const char *req, void *arg) {
  webview_bind_context *context = (webview_bind_context *)arg;
  PyObject *func = context->func;
  PyObject *arglist = Py_BuildValue("(s)", req);
  PyObject *result = PyObject_CallObject(func, arglist);
  Py_ssize_t size;
  Py_DECREF(arglist);
  if (result == NULL) {
    webview_return(context->webview, seq, -1,
                   (char *)"{\"error\":\"Something went wrong\"}");
    return;
  }
  const char *ptr = PyUnicode_AsUTF8AndSize(result, &size);
  Py_DECREF(result);
  if (!ptr) {
    webview_return(context->webview, seq, -1,
                   (char *)"{\"error\":\"Something went wrong\"}");
    return;
  }
  webview_return(context->webview, seq, 0, ptr);
}

static PyObject *Webview_bind(WebviewObject *self, PyObject *args,
                              PyObject *kwds) {
  PyObject *func;
  const char *name;
  static char *kwlist[] = {(char *)"name", (char *)"func", NULL };
  if (!PyArg_ParseTupleAndKeywords(args, kwds, "sO", kwlist, &name, &func))
    return NULL;
  if (!PyCallable_Check(func)) {
    PyErr_SetString(PyExc_TypeError, "func parameter must be callable");
    return NULL;
  }
  Py_XINCREF(func);
  webview_bind_context *context =
      (webview_bind_context *)malloc(sizeof(webview_bind_context));
  if (context == NULL)
    return PyErr_NoMemory();
  webview_bind_contexts *contexts =
      (webview_bind_contexts *)malloc(sizeof(webview_bind_contexts));
  if (contexts == NULL) {
    free(context);
    return PyErr_NoMemory();
  }
  contexts->context = context;
  contexts->next = self->contexts;
  self->contexts = contexts;
  context->webview = self->webview;
  context->func = func;
  webview_bind(self->webview, name, &run_bind_func, (void *)context);
  Py_RETURN_NONE;
}

static PyMethodDef Webview_methods[] = {
  { "run",       (PyCFunction)Webview_run,
    METH_NOARGS, PyDoc_STR("Runs the main loop until it's terminated.") },
  { "navigate",
    (PyCFunction)Webview_navigate,
    METH_VARARGS | METH_KEYWORDS,
    PyDoc_STR("Navigates webview to the given URL.") },
  { "set_title", (PyCFunction)Webview_set_title, METH_VARARGS | METH_KEYWORDS,
    PyDoc_STR("Updates the title of the native window. Must be called from "
              "the UI thread.") },
  { "set_size",                   (PyCFunction)Webview_set_size,
    METH_VARARGS | METH_KEYWORDS, PyDoc_STR("Updates native window size.") },
  { "set_html",                   (PyCFunction)Webview_set_html,
    METH_VARARGS | METH_KEYWORDS, PyDoc_STR("Sets webview HTML directly.") },
  { "terminate", (PyCFunction)Webview_terminate,
    METH_NOARGS, PyDoc_STR("Stops the main loop.") },
  { "bind", (PyCFunction)Webview_bind, METH_VARARGS | METH_KEYWORDS,
    PyDoc_STR("Binds a callback so that it will appear under the given name "
              "as a global JavaScript function.") },
  { "dispatch",
    (PyCFunction)Webview_dispatch,
    METH_VARARGS | METH_KEYWORDS,
    PyDoc_STR("Posts a function to be executed on the main thread.") },
  { "init", (PyCFunction)Webview_run_init, METH_VARARGS | METH_KEYWORDS,
    PyDoc_STR("Injects JavaScript code at the initialization of the new "
              "page.") },
  { "eval",
    (PyCFunction)Webview_eval,
    METH_VARARGS | METH_KEYWORDS,
    PyDoc_STR("Evaluates arbitrary JavaScript code.") },
  { NULL, NULL } /* sentinel */
};

static PyTypeObject Webview_Type = {
  /* The ob_type field must be initialized in the module init function
   * to be portable to Windows without using C++. */
  PyVarObject_HEAD_INIT(NULL, 0) "_webview.Webview", /*tp_name*/
  sizeof(WebviewObject),                             /*tp_basicsize*/
  0,                                                 /*tp_itemsize*/
  /* methods */
  (destructor)Webview_dealloc, /*tp_dealloc*/
  0,                           /*tp_vectorcall_offset*/
  (getattrfunc)0,              /*tp_getattr*/
  (setattrfunc)0,              /*tp_setattr*/
  0,                           /*tp_as_async*/
  0,                           /*tp_repr*/
  0,                           /*tp_as_number*/
  0,                           /*tp_as_sequence*/
  0,                           /*tp_as_mapping*/
  0,                           /*tp_hash*/
  0,                           /*tp_call*/
  0,                           /*tp_str*/
  (getattrofunc)0,             /*tp_getattro*/
  0,                           /*tp_setattro*/
  0,                           /*tp_as_buffer*/
  Py_TPFLAGS_DEFAULT,          /*tp_flags*/
  0,                           /*tp_doc*/
  0,                           /*tp_traverse*/
  0,                           /*tp_clear*/
  0,                           /*tp_richcompare*/
  0,                           /*tp_weaklistoffset*/
  0,                           /*tp_iter*/
  0,                           /*tp_iternext*/
  Webview_methods,             /*tp_methods*/
  0,                           /*tp_members*/
  0,                           /*tp_getset*/
  0,                           /*tp_base*/
  0,                           /*tp_dict*/
  0,                           /*tp_descr_get*/
  0,                           /*tp_descr_set*/
  0,                           /*tp_dictoffset*/
  (initproc)Webview_init,      /*tp_init*/
  0,                           /*tp_alloc*/
  Webview_new,                 /*tp_new*/
  0,                           /*tp_free*/
  0,                           /*tp_is_gc*/
};

static int webview_exec(PyObject *m) {
  /* Finalize the type object including setting type of the new type
   * object; doing it here is required for portability, too. */
  if (PyType_Ready(&Webview_Type) < 0) {
    Py_XDECREF(m);
    return -1;
  }
  Py_INCREF(&Webview_Type);
  PyModule_AddObject(m, "Webview", (PyObject *)&Webview_Type);
  PyModule_AddIntConstant(m, "HINT_NONE", WEBVIEW_HINT_NONE);
  PyModule_AddIntConstant(m, "HINT_MIN", WEBVIEW_HINT_MIN);
  PyModule_AddIntConstant(m, "HINT_MAX", WEBVIEW_HINT_MAX);
  PyModule_AddIntConstant(m, "HINT_FIXED", WEBVIEW_HINT_FIXED);
  return 0;
}

static PyMethodDef webview_methods[] = { { NULL } };

static struct PyModuleDef_Slot webview_slots[] = {
  { Py_mod_exec, (void *)webview_exec }, { 0, NULL },
};

static struct PyModuleDef _webview = {
  PyModuleDef_HEAD_INIT,                                "_webview",
  PyDoc_STR("Low level bindings to c webview library"), 0,
  webview_methods,                                      webview_slots,
  NULL,                                                 NULL,
  NULL
};

PyMODINIT_FUNC PyInit__webview(void) { return PyModuleDef_Init(&_webview); }
