# MIT License

# Copyright (c) 2021 Peter Debelak

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
"""Python bindings for webview library"""
import json
from enum import Enum
from typing import Callable

import webview._webview as _webview


class Hint(Enum):
    """Enum of hint options used for set_size"""

    NONE = _webview.HINT_NONE
    MIN = _webview.HINT_MIN
    MAX = _webview.HINT_MAX
    FIXED = _webview.HINT_FIXED


class Webview:
    """Class representing a single webview"""

    def __init__(self, debug: bool = False):
        self._webview = _webview.Webview(debug=debug)

    def run(self):
        """Runs the main loop until it's terminated."""
        self._webview.run()

    def navigate(self, url: str):
        """
        Navigates webview to the given URL. URL may be a data URI, i.e.
        "data:text/html,<html>...</html>". It is often ok not to
        url-encode it properly, webview will re-encode it for you.
        """
        self._webview.navigate(url)

    def set_html(self, html: str):
        """Sets html. Calls "navigate", taking care of escaping."""
        self._webview.set_html(html)

    def set_title(self, title: str):

        """
        Updates the title of the native window.
        """

        def do_title_set():
            self._webview.set_title(title)

        self.dispatch(do_title_set)

    def terminate(self):
        """
        Stops the main loop. It is safe to call this function from another
        other background thread.
        """
        self._webview.terminate()

    def set_size(self, height: int, width: int, hints: Hint):
        """Updates native window size."""
        self._webview.set_size(height, width, hints.value)

    def bind(self, name: str, func: Callable):
        """
        Binds a callback so that it will appear under the given name as a
        global JavaScript function. Return value must be json
        serializable.
        """

        def run_bind(arglist: str) -> str:
            args = json.loads(arglist)
            assert type(args) is list, "Expected bind arguments to be a list"
            result = func(*args)
            return json.dumps(result)

        self._webview.bind(name, run_bind)

    def set_bindings(self, **kwargs: Callable):
        for name, func in kwargs.items():
            self.bind(name, func)

    def dispatch(self, func: Callable):
        """Posts a function to be executed on the main thread."""
        self._webview.dispatch(func)

    def init(self, js: str):
        """
        Injects JavaScript code at the initialization of the new page.
        Every time the webview will open a the new page - this
        initialization code will be executed. It is guaranteed that
        code is executed before window.onload.
        """
        self._webview.init(js)

    def eval(self, js: str):
        """
        Evaluates arbitrary JavaScript code. Evaluation happens
        asynchronously, also the result of the expression is ignored.
        Use RPC bindings if you want to receive notifications about
        the results of the evaluation.
        """
        self._webview.eval(js)
