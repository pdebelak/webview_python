import os
import subprocess

from setuptools import Extension, setup

if hasattr(os, "uname"):
    OSNAME = os.uname()[0]
else:
    OSNAME = "Windows"

if OSNAME == "Linux":

    def pkgconfig(flags):
        result = subprocess.run(
            ["pkg-config", flags, "gtk+-3.0", "webkit2gtk-4.0"],
            capture_output=True,
            check=True,
        )
        return result.stdout.decode("utf-8").split()

    define_macros = [("WEBVIEW_GTK", "1")]
    extra_cflags = [*pkgconfig("--cflags"), "-std=c++11"]
    extra_ldflags = pkgconfig("--libs")
elif OSNAME == "Darwin":
    define_macros = [("WEBVIEW_COCOA", "1")]
    extra_cflags = ["-std=c++11"]
    extra_ldflags = ["-framework", "WebKit"]
elif OSNAME == "Windows":
    define_macros = [("WEBVIEW_WINAPI", "1")]
    extra_cflags = ["-mwindows"]
    extra_ldflags = ["-L./submodules/webview/dll/x64,", "-lwebview", "-lWebView2Loader"]
else:
    raise RuntimeError(f"Unknown OS: {OSNAME}")

setup(
    name="webview",
    version="0.2.0",
    description="Python webview bindings",
    author="Serge Zaitsev",
    author_email="zaitsev.serge@gmail.com",
    maintainer="Peter Debelak",
    maintainer_email="pdebelak@gmail.com",
    url="https://gitlab.com/pdebelak/webview_python",
    keywords=[],
    license="MIT",
    classifiers=[],
    packages=["webview"],
    ext_modules=[
        Extension(
            "webview._webview",
            sources=["webview/_webview.cc"],
            define_macros=define_macros,
            extra_compile_args=extra_cflags,
            extra_link_args=extra_ldflags,
        )
    ],
    package_data={"": ["webview/_webview.cc", "submodules/webview/webview.h"]},
)
