IMAGE:=webview_python
DOCKER_RUN=docker run -t $(IMAGE)

all: lint test

lint: lint_c lint_python

test:
	python3 -m unittest discover tests/

lint_c:
	scripts/lint_c

lint_python:
	scripts/lint_python

docker_all: docker_lint docker_test

docker_lint: docker_lint_c docker_lint_python

docker_test: docker_image
	$(DOCKER_RUN) sh -c "xvfb-run python3 -m unittest discover /webview/build/tests/"

docker_lint_c: docker_image
	$(DOCKER_RUN) /webview/build/scripts/lint_c

docker_lint_python: docker_image
	$(DOCKER_RUN) /webview/build/scripts/lint_python

docker_image:
	docker build -t $(IMAGE) .

.PHONY: test lint lint_c lint_python docker_lint docker_test docker_lint_c docker_lint_python docker_image
